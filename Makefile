.PHONY: all clean clean-configure mrproper install

include Makeconf

TOP=$(abspath .)
FREESTANDING_CFLAGS+=-isystem $(TOP)/nolibc/include
BUILD=$(BUILD_PATH)

ifeq ($(OCAML_GTE_4_07_0),yes)
FREESTANDING_LIBS=$(BUILD_PATH)/ocaml/asmrun/libasmrun.a \
		  $(BUILD_PATH)/nolibc/libnolibc.a
else
FREESTANDING_LIBS=$(BUILD_PATH)/ocaml/asmrun/libasmrun.a \
		  $(BUILD_PATH)/ocaml/otherlibs/libotherlibs.a \
		  $(BUILD_PATH)/nolibc/libnolibc.a
endif

all: $(FREESTANDING_LIBS) ocaml-freestanding.pc

nolibc: $(BUILD_PATH)/nolibc/libnolibc.a

asmrun: $(BUILD_PATH)/ocaml/asmrun/libasmrun.a

$(BUILD_PATH)/ocaml/Makefile:
	mkdir -p build
	cp -r $(OCAML_SRC_DIR) $(BUILD_PATH)/ocaml

$(BUILD_PATH)/ocaml/config/Makefile: $(BUILD_PATH)/ocaml/Makefile
ifeq ($(OCAML_GTE_4_06_0),yes)
	cp config/s.h $(BUILD_PATH)/ocaml/byterun/caml/s.h
	cp config/m.$(BUILD_ARCH).h $(BUILD_PATH)/ocaml/byterun/caml/m.h
else
	cp config/s.h $(BUILD_PATH)/ocaml/config/s.h
	cp config/m.$(BUILD_ARCH).h $(BUILD_PATH)/ocaml/config/m.h
endif
	cp config/Makefile.$(BUILD_OS).$(BUILD_ARCH) $(BUILD_PATH)/ocaml/config/Makefile

# Needed for OCaml >= 4.03.0, triggered by OCAML_EXTRA_DEPS via Makeconf
$(BUILD_PATH)/ocaml/byterun/caml/version.h: $(BUILD_PATH)/ocaml/config/Makefile
	$(BUILD_PATH)/ocaml/tools/make-version-header.sh > $@

OCAML_CFLAGS=-O2 -fno-strict-aliasing -fwrapv -Wall -USYS_linux -DHAS_UNISTD $(FREESTANDING_CFLAGS) -mcmodel=medany
$(BUILD_PATH)/ocaml/asmrun/libasmrun.a: $(BUILD_PATH)/ocaml/config/Makefile $(OCAML_EXTRA_DEPS)
ifeq ($(OCAML_GTE_4_06_0),yes)
	$(MAKE) -C $(BUILD_PATH)/ocaml/asmrun \
	    UNIX_OR_WIN32=unix \
	    CFLAGS="$(OCAML_CFLAGS)" \
	    libasmrun.a
else
	$(MAKE) -C $(BUILD_PATH)/ocaml/asmrun \
	    UNIX_OR_WIN32=unix \
	    NATIVECCCOMPOPTS="$(OCAML_CFLAGS)" \
	    NATIVECCPROFOPTS="$(OCAML_CFLAGS)" \
	    libasmrun.a
endif

$(BUILD_PATH)/ocaml/otherlibs/libotherlibs.a: $(BUILD_PATH)/ocaml/config/Makefile
ifeq ($(OCAML_GTE_4_06_0),yes)
	$(MAKE) -C $(BUILD_PATH)/ocaml/otherlibs/bigarray \
	    OUTPUTOBJ=-o \
	    CFLAGS="$(FREESTANDING_CFLAGS) -DIN_OCAML_BIGARRAY -I../../byterun" \
	    bigarray_stubs.o mmap_ba.o mmap.o
	$(AR) rcs $@ \
	    $(BUILD_PATH)/ocaml/otherlibs/bigarray/bigarray_stubs.o \
	    $(BUILD_PATH)/ocaml/otherlibs/bigarray/mmap_ba.o \
	    $(BUILD_PATH)/ocaml/otherlibs/bigarray/mmap.o
else
	$(MAKE) -C $(BUILD_PATH)/ocaml/otherlibs/bigarray \
	    CFLAGS="$(FREESTANDING_CFLAGS) -I../../byterun" \
	    bigarray_stubs.o mmap_unix.o
	$(AR) rcs $@ \
	    $(BUILD_PATH)/ocaml/otherlibs/bigarray/bigarray_stubs.o \
	    $(BUILD_PATH)/ocaml/otherlibs/bigarray/mmap_unix.o
endif

$(BUILD_PATH)/nolibc/Makefile:
	mkdir -p $(BUILD_PATH)
	cp -r nolibc $(BUILD_PATH)
ifeq ($(OCAML_GTE_4_07_0),yes)
	echo '/* automatically added by configure.sh */' >> $(BUILD_PATH)/nolibc/stubs.c
	echo 'STUB_ABORT(caml_ba_map_file);' >> $(BUILD_PATH)/nolibc/stubs.c
endif

NOLIBC_CFLAGS=$(FREESTANDING_CFLAGS)
$(BUILD_PATH)/nolibc/libnolibc.a: $(BUILD_PATH)/nolibc/Makefile 
	$(MAKE) -C $(BUILD_PATH)/nolibc \
	    "FREESTANDING_CFLAGS=$(NOLIBC_CFLAGS)" \
	    "SYSDEP_OBJS=$(NOLIBC_SYSDEP_OBJS)"

ocaml-freestanding.pc: ocaml-freestanding.pc.in 
	sed -e 's!@@PKG_CONFIG_DEPS@@!$(PKG_CONFIG_DEPS)!' \
	    -e 's!@@PKG_CONFIG_EXTRA_LIBS@@!$(PKG_CONFIG_EXTRA_LIBS)!' \
	    ocaml-freestanding.pc.in > $@

install: all
	./install.sh

uninstall:
	./uninstall.sh

clean-configure:
	rm -rf config Makeconf ocaml-freestanding.pc Buildconf

clean:
	rm -rf $(BUILD_PATH)/nolibc $(BUILD_PATH)/ocaml/

mrproper: clean clean-configure
